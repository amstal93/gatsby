## Description

Docker image to run Gatsby dev tools (`gatsby`, `yarn` ...)

## Usage

```bash
docker run -it --rm -v $(pwd):/app -w /app --network host kalmac/gatsby gatsby develop 
```

[See Dockerfile here](https://gitlab.com/kalmac/gatsby/blob/master/Dockerfile)

## Best usage

With [Dockerenv](https://gitlab.com/kalmac/dockerenv), type `dockerenv` in a 
configured project to access `gatsby` dev tools.

```bash
dockerenv
yarn
gatsby develop
```
